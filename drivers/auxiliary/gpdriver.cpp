/*******************************************************************************
  Copyright(c) 2012 Jasem Mutlaq. All rights reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; see the file COPYING.LIB.  If not, write to
  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.

  The full GNU General Public License is included in this distribution in the
  file called LICENSE.
*******************************************************************************/

#include "gpdriver.h"
#include <wiringPi.h>

using namespace std;	

GPUSBDriver::GPUSBDriver()
{
    wiringPiSetup();
    debug = false;
}

GPUSBDriver::~GPUSBDriver()
{

}

bool GPUSBDriver::Connect()
{
  std::cout << " Setting GPIO pin directions\n" << std::endl;
  pinMode(RA_plus, OUTPUT);
  pinMode(RA_minus, OUTPUT);
  pinMode(DEC_plus, OUTPUT);
  pinMode(DEC_minus, OUTPUT);
  std::cout << " GPIO ready\n" << std::endl;
  return 1;
}

bool GPUSBDriver::Disconnect()
{
    return true;
}

bool GPUSBDriver::startPulse(int direction)
{
    int rc = 0;

    switch (direction)
    {
      case GPIOST4_NORTH:
        if (debug) IDLog("Start North\n");
          digitalWrite(RA_plus, HIGH);
        break;

        case GPIOST4_WEST:
        if (debug) IDLog("Start West\n");
          digitalWrite(DEC_plus, HIGH);
        break;

        case GPIOST4_SOUTH:
        if (debug) IDLog("Start South\n");
          digitalWrite(RA_minus, HIGH);
        break;

        case GPIOST4_EAST:
        if (debug) IDLog("Start East\n");
          digitalWrite(DEC_minus, HIGH);
        break;
    }
    return 1;
}

bool GPUSBDriver::stopPulse(int direction)
{
    switch (direction)
    {
        case GPIOST4_NORTH:
        if (debug) IDLog("Stop North\n");
          digitalWrite(RA_plus, LOW);
        break;

        case GPIOST4_WEST:
        if (debug) IDLog("Stop West\n");
          digitalWrite(DEC_plus, LOW);
        break;

        case GPIOST4_SOUTH:
        if (debug) IDLog("Stop South\n");
          digitalWrite(RA_minus, LOW);
        break;

        case GPIOST4_EAST:
        if (debug) IDLog("Stop East\n");
          digitalWrite(DEC_minus, LOW);
        break;
    }
    return 1;
}
